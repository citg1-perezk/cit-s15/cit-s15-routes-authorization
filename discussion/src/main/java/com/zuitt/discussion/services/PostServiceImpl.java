// contains the business logic concerned with a particular object   in the class
package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// will allow us to use the CRUD repository methods inherited from the CRUDRepository
@Service
public class PostServiceImpl implements PostService{
    //An object cannot be instantiated from interfaces.
    //@Autowired allow us to use the interface as if it was an instance marked as @Repository contains methods for database manipulation
    @Autowired
    private PostRepository postRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;
    //Create Post
    public void createPost(String stringToken,Post post){
        // Retrieve the "User" object using the extracted username from the JWT token
        User author= userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost=new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);

    }
    //Get all posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
    //delete post
    public ResponseEntity deletePost(Long id, String stringToken){/*
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);*/
        Post postForDeleting=postRepository.findById(id).get();
        String postAuthorName=postForDeleting.getUser().getUsername();
        String authenticatedUserName=jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUserName.equals(postAuthorName)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }
    //update post
  public ResponseEntity updatePost(Long id, String stringToken, Post post){
        //find post to update
       /* Post postForUpdate=postRepository.findById(id).get();
        //Saving and Updating the title and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());
        postRepository.save(postForUpdate);
        return new ResponseEntity("Post updated successfully", HttpStatus.OK);*/
        Post postForUpdating =postRepository.findById(id).get();
        //Get the "author" of the specific post.
        String postAuthorName=postForUpdating.getUser().getUsername();
        //Get the "Username" from the stringToken to compare it with the usernameof the current post being edited.
      String authenticatedUserName=jwtToken.getUsernameFromToken(stringToken);
      //check if the username of the authenticated user matches the username of the post's author.
      if(authenticatedUserName.equals(postAuthorName)){
          postForUpdating.setTitle(post.getTitle());
          postForUpdating.setContent(post.getContent());
          postRepository.save(postForUpdating);
          return new ResponseEntity<>("Post updated successfully",HttpStatus.OK);
      }
      else{
          return new ResponseEntity<>("You are not authorized to edit this post.",HttpStatus.UNAUTHORIZED);
      }

    }
    // Get user's posts
    public Iterable<Post> getMyPosts(String stringToken){
        User author=userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }

}
